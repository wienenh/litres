class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.integer :origID
      t.string :source
      t.string :title
      t.string :authors
      t.string :url
      t.text :abstract

      t.timestamps null: false
    end
  end
end
