class CreateReviews < ActiveRecord::Migration
  def change
    create_table :reviews do |t|
      t.string :result
      t.references :user, index: true, foreign_key: true
      t.references :article, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
