# all numbers were generated from random.org, each number is the start of a 10 article series (so 322 means articles with ids 322 - 331)

def fill_reviews(starting_points, user)
  starting_points.each do |starting_point|
    (0..19).each do |i|
      review = Review.new
      review.article_id = starting_point + i
      review.user_id = user.id
      review.save
    end
  end
end
    

# Faiza - 200 articles to review
faiza = User.find_by_name('Faiza')
starting_points = [1136, 919, 1387, 153, 1588, 956, 1684, 126, 1158, 39]
fill_reviews(starting_points, faiza)

# Eelco - 200 articles to review
eelco = User.find_by_name('Eelco')
starting_points = [378, 356, 516, 192, 851, 1724, 1076, 1205, 443, 764]
fill_reviews(starting_points, eelco)

# Roel - 100 articles to review - half overlapping with some ranges of Faiza and Eelco
roel = User.find_by_name('Roel')
starting_points = [1126, 506, 1578, 1066, 29]
fill_reviews(starting_points, roel)
