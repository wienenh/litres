connection = ActiveRecord::Base.connection
connection.tables.each do |table|
  connection.execute("truncate #{table} cascade") unless table == "schema_migrations"
end
# - IMPORTANT: SEED DATA ONLY
# - DO NOT EXPORT TABLE STRUCTURES
# - DO NOT EXPORT DATA FROM `schema_migrations`
sql = File.read('db/data-utf8.sql')
statements = sql.split(/;$/)
statements.pop  # the last empty statement

ActiveRecord::Base.transaction do
  statements.each do |statement|
    connection.execute(statement)
  end
end