Rails.application.routes.draw do
  
  devise_for :users
  root to: "articles#index"
  resources :users
  
  get '/articles/unrated', to: 'articles#unrated'
  get '/articles/yesses',  to: 'articles#yesses'
  get '/articles/numbered', to: 'articles#sorted_by_id'
  
  resources :articles
  resources :differences

  get '/differences/nose', to: 'differences#nose'
  
  resources :reviews do
    post "read"
    post "ignore"
    post "duplicate"
  end
end
