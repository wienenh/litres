class DifferencesController < ApplicationController
  def all
    @differences = Difference.all.paginate(page: params[:page], per_page: 30)
  end

  def index
    @differences = Difference.all.where('hans = \'N\'').paginate(page: params[:page], per_page: 30)
  end
end
    