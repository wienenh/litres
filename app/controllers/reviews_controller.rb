class ReviewsController < ApplicationController
#   before_action :set_review, only: [:read, :ignore]
  before_action :set_review_by_review_id, only: [:read, :ignore, :duplicate]
  before_action :authenticate_user!
  
  def show
  end
    
  # PATCH/PUT /articles/1
  # PATCH/PUT /articles/1.json
  def read
    if @review.result == "Y"
      @review.result = ""
    else
      @review.result = "Y"
    end
    save_and_update_buttons
  end

  def ignore
    if @review.result == "N"
      @review.result = ""
    else
      @review.result = "N"
    end
    save_and_update_buttons
  end
  
  def duplicate
    if @review.result[0] == "D"
      @review.result = ""
    else

      @review.result = "D#{params[dup_to_sym]}"
    end
    save_and_update_buttons
  end

  private
  
    def save_and_update_buttons
      @article = @review.article
      respond_to do |format|
        if @review.save
          format.html { redirect_to @review, notice: 'Review was successfully updated.' }
          format.js { render 'update_buttons', layout: false }
          format.json { render :show, status: :ok, location: @review }
        else
          format.html { render :edit }
          format.json { render json: @review.errors, status: :unprocessable_entity }
        end
      end
    end
    
    # Use callbacks to share common setup or constraints between actions.
    def set_review
      @review = Review.find(params[:id])
    end
    
    def set_review_by_review_id
      @review = Review.find(params[:review_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def review_params
      params.require(:review).permit(:result, dupToSym)
    end
    
    def dup_to_sym
      "text-duplicate-#{@review.id}".to_sym
    end
end
