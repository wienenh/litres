json.array!(@articles) do |article|
  json.extract! article, :id, :origID, :source, :title, :authors, :url, :abstract
  json.url article_url(article, format: :json)
end
