class Difference < ActiveRecord::Base
  self.table_name = 'differences'
  
  protected
  def readonly?
    true
  end
end