class Review < ActiveRecord::Base
  belongs_to :user
  belongs_to :article
  
  def self.read(user=nil)
    get_reviews_by_condition user, "result = 'Y'"
  end
  
  def self.ignore(user)
    get_reviews_by_condition user, "result = 'N'"
  end
  
  def self.duplicates(user)
    get_reviews_by_condition user, "result like 'D%'"
  end
  
  def self.to_be_done(user)
    get_reviews_by_condition user, "result = ''"
  end
  
  def self.get_reviews_by_condition(user, condition)
    if user != nil
      self.where("user_id = #{user.id}").where(condition).count
    else
      self.where(condition).count
    end
  end
end
