class Article < ActiveRecord::Base
  attr_accessor :authors
  
  has_many :reviews
  has_many :users, through: :reviews
  
  def authors
    encd self[:authors]
  end
  
  def abstract
    encd self[:abstract]
  end
  
  def encd(string)
    string = "«Not available»" if string.nil? or string == ""
    string
  end
  
  def for_user(user)
    self.reviews.find_by_user_id(user)
  end
end
