class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  # :validatable
  
  has_many :reviews
  has_many :articles, through: :reviews 
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable
end
